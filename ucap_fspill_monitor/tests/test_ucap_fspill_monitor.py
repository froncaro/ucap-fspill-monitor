"""
High-level tests for the  package.

"""

import ucap_fspill_monitor


def test_version():
    assert ucap_fspill_monitor.__version__ is not None
