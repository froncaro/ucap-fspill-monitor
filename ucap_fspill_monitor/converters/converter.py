import logging
from typing import List
import numpy as np
from ucap_fspill_monitor.utils.fft import *
from ucap_fspill_monitor.utils.process import *



from ucap.common import (
    AcquiredParameterValue,
    Event,
    FailSafeParameterValue,
    ValueHeader,
    ValueType,
)
from ucap.common.context import (
    configuration,
    device_name,
    transformation_name,
    published_property_name,
)

logger = logging.getLogger(__name__)

###def convert(event: Event) -> Optional[FailSafeParameterValue]:
def convert(event: Event) -> List[FailSafeParameterValue]:

    logger = logging.getLogger(__name__)

    header = event.trigger_value.header if event.trigger_value else ValueHeader()

    trigger_stamp = event.get_value("scope_info").value.get_value("lastTriggerTimestamp")

    ch1_apv: AcquiredParameterValue = event.get_value("ch1_data").value

    sampling_f, fdata_type = ch1_apv.get_value_and_type('samplingFrequency')

    ch1_data, data_type = ch1_apv.get_value_and_type('values')
    #logger.info(f'This are the first Ch1 elements {ch1_data[:20]} of type {data_type}')

    output_scope_apv = AcquiredParameterValue(f'{device_name}/scope', header)
    output_ch_apv = AcquiredParameterValue(f'{device_name}/data',     header)

    output_ch_processed_apv = AcquiredParameterValue(f'{device_name}/data_processed')

    ######
    output_scope_apv.update_value(f'sampl_freq', sampling_f, fdata_type)
    output_scope_apv.update_value(f'trig_stamp', trigger_stamp, ValueType.INT)

    output_ch_apv.update_value(f"values", ch1_data, data_type)

    ##process data
    dt = 1. / sampling_f
    dt_avg = 1e-3
    npt_raw = len(ch1_data)

    npt_w = int(dt_avg/ dt)
    ch1_data = np.array(ch1_data)

    #try:
    data_averaged = np.array( running_average(ch1_data, npt_w)[::npt_w].astype(float) )
    #except:
    #data_averaged = np.array([9999.])


    vf, vspectrum = myFFT(ch1_data,  dt )

    f1 = 0
    f2 = 200

    vf_lf, vspectrum_lf = spectrum_zoom(vf, vspectrum,f1,f2)

    output_ch_processed_apv.update_value(f'data_averaged', data_averaged.tolist(), ValueType.FLOAT_ARRAY)
    output_ch_processed_apv.update_value(f'vf_low_freq', vf_lf.tolist(), ValueType.FLOAT_ARRAY)
    output_ch_processed_apv.update_value(f'vspectrum_low_freq', vspectrum_lf.tolist(), ValueType.FLOAT_ARRAY)

    output_ch_processed_apv.update_value(f'sampl_period', dt, ValueType.DOUBLE)
    output_ch_processed_apv.update_value(f'sampl_period_averaged', dt_avg, ValueType.DOUBLE)


    f_output_scope_apv = FailSafeParameterValue(output_scope_apv)
    f_output_ch_apv = FailSafeParameterValue(output_ch_apv)
    f_output_ch_processed_apv = FailSafeParameterValue(output_ch_processed_apv)

    return [f_output_scope_apv, f_output_ch_apv, f_output_ch_processed_apv]
