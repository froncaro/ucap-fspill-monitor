import numpy as np

def myFFT_200MHZ(data, tsampling):
    ps = np.abs(np.fft.fft(data)) ** 2
    freqs = np.fft.fftfreq(data.size, tsampling)

    idx = np.argsort(freqs)
    mask = (freqs[idx] >= 0) & (freqs[idx] > 199.8 * 1e6) & (freqs[idx] < 201.2 * 1e6)

    return (np.concatenate([np.array([0]), freqs[idx][mask]]), np.concatenate([[ps[0]], ps[idx][mask]]))


def myFFT(data, tsampling):
    ps = np.abs(np.fft.fft(data)) ** 2
    freqs = np.fft.fftfreq(data.size, tsampling)

    idx = np.argsort(freqs)
    mask = freqs[idx] >= 0
    # print (freqs, len(freqs), len(freqs[mask]),freqs[mask])
    return (freqs[idx][mask], ps[idx][mask])

