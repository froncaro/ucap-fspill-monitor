import numpy as np

def running_average(raw_data, w):
    return np.convolve(raw_data, np.ones(w), 'valid') / w


def spectrum_zoom(vf, spectrum, f1, f2):
    spectrum=np.array(spectrum)
    vf = np.array(vf)
    mask =  (vf>f1) & (vf<f2)

    return vf[mask],spectrum[mask]